
let orderSchema = {
    username: String,
    name: String,
    address: String,
    phone: Number,
    pay: String,
    stade: String,
    food: [],
    bill: Number,
    date: String,
    userKey : Number,
};

export default orderSchema;