
const productSchema = {
    name : String,
    imgUrl : String,
    ingredients : String,
    score : Number,
    usersAlreadyScored : [],
    price : Number
}


export default productSchema;

