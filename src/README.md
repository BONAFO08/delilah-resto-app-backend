
Repositorio del proyecto : https://gitlab.com/BONAFO08/sp2

Todos los comandos aquí dictados esta pensados para ser ejecutados desde la carpeta 'src'


1. Instalación:

- Instalar Node 14.17.3
- Instalar MongoDB 5.0.2
- Instalar  Redis 3.2.100


Comandos necesarios para finalizar la instalación:
- npm i

Antes de iniciar el servidor, cambiar los siguientes capos del archivo '.env':

- MONGODB_MOVIE_DB_NAME= (Colocar aquí el nombre de la base de datos)
- JWT_SECRET= (Colocar aquí un código para generar los tokens de logeo)



2. Comandos internos:

- npm start  (Iniciar el servidor)
- npm test (Iniciar el test de registro de usuarios // Requiere que el servidor este operativo) 


3. Administrador:

Cuenta admin default :

- username: admin
- password: a1d2m3i4n5


Recomendación de eliminar esta cuenta una vez se haya creado el primer administrador.
