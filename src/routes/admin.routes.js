// const express = require('express');
// const router = express.Router();
// const userMiddle = require('../middle/user.middle')

import  express  from "express";
import {delUserAdmin, modRights, showUsers, usersBan} from '../middle/user.middle.js'
const router = express.Router();


/**
 * @swagger
 * /admin/connectedUsers:
 *  get:
 *    tags: 
 *      [Admin]
 *    summary: Ver Usuarios logeados
 *    description: Permite ver los usuario actualmente conectados
 *    parameters:
 *    - name: authorization
 *      description: Token de admin
 *      in: header
 *      required: false
 *      type: string
 *    responses:
 *            200:
 *                description: Se muestran todos los usuarios logeados
 *            403:
 *                description: Credenciales incorrectas
 * 
 */


 router.get("/admin/connectedUsers", (req, res) => {
    showUsers(req,res);
});

/**
 * @swagger
 * /admin/rights: 
 *  post:
 *    tags: 
 *      [Admin]
 *    summary: Cambiar privilegios
 *    description: Modifica los permisos de un usuario (Requiere re-login)
 *    parameters:
 *    - name: authorization
 *      description: Token de admin
 *      in: header
 *      required: false
 *      type: string
 *    - name: name
 *      description: Nombre de usuario o correo electrónico del usuario a cambiar
 *      in: formData
 *      required: false
 *      type: string
 *    - name:  rights
 *      description: Nivel de privilegios del usuario 
 *      in: query
 *      type: string
 *      enum: [client,admin]
 *      required: false
 *    responses:
 *            200:
 *                description: Privilegios modificados con exito
 *            403:
 *                description: Credenciales incorrectas
 *            404:
 *                description: Usuario no encontrado
 *            400:
 *                description: Error al validar los datos 
 */

 router.post("/admin/rights", (req, res) => {
    modRights(req,res);
    
});

/**
 * @swagger
 * /admin/usersBan: 
 *  post:
 *    tags: 
 *      [Admin]
 *    summary: Banear o desbanear usuario
 *    description: Banea o le quita el baneo a un usuario 
 *    parameters:
 *    - name: authorization
 *      description: Token de admin
 *      in: header
 *      required: false
 *      type: string
 *    - name: name
 *      description: Nombre de usuario o correo electrónico del usuario
 *      in: formData
 *      required: false
 *      type: string
 *    - name:  ban
 *      description: Banear usuario o quitarle el baneo
 *      in: query
 *      type: string
 *      enum: [banear usuario,desbanear usuario]
 *      required: false
 *    responses:
 *            200:
 *                description: Baneo quitado / aplicado exitosamente
 *            403:
 *                description: Credenciales incorrectas
 *            404:
 *                description: Usuario no encontrado
 *            400:
 *                description: Error al validar los datos 
 */

 router.post("/admin/usersBan", (req, res) => {
    usersBan(req,res);
});


/**
 * @swagger
 * /admin/adminDeleteUser: 
 *  delete:
 *    tags: 
 *      [Admin]
 *    summary: Eliminar la cuenta de un usuario
 *    description: Permite al administrador eliminar la cuenta del usuario proporcionado
 *    parameters:
 *    - name: authorization
 *      description: Token de admin
 *      in: header
 *      required: false
 *      type: string
 *    - name: name
 *      description: Nombre de usuario o correo electrónico del usuario
 *      in: formData
 *      required: false
 *      type: string
 *    responses:
 *            200:
 *                description: Cuenta eliminada
 *            403:
 *                description: Credenciales incorrectas
 *            404:
 *                description: Usuario no encontrado
 *            400:
 *                description: Error al validar los datos 
 */
router.delete("/admin/adminDeleteUser", (req, res) => {
    delUserAdmin(req,res);
});


export {router}
