// const search = require('./search')
// const db = require('../config/conexion.config');
// const productRedis = require('./redis.product');

import { Product } from '../config/conexion.config.js';
import { newCacheProduct,modifyACacheProduct,deleteACacheProduct } from './redis.product.js';
import { findById, findByName } from './search.js';


//Create a new product and save it in the Data Base
const createProduct = async (data) => {

    let validator = await findByName(Product, data.name);

    let msj;

    (validator != false) ? (msj = `El nombre de producto ya esta en uso`) : ('');


    if (validator == false) {

        const temp = await new Product({
            name: data.name,
            price: data.price,
            imgUrl: data.imgUrl,
            ingredients : data.ingredients,
            score : 0,
            usersAlreadyScored : [],
        }
        );
        
        const result = await temp.save();
        newCacheProduct(result);
        return result;
    } else {
        throw msj;
    }
};

//Modify cero o more attributes of a user
const modifyAProduct = async (data, id) => {

    let msj = {
        txt: '',
        status: 409,
    };

    (id == undefined) ? (id = '') : (id = id.toString().trim());

    let validator = await findByName(Product, data.name);
    let validator3 = await findById(Product, id);;


    (validator != false) ? (msj.txt = `El nombre de producto ya esta en uso`) : ('');
    (validator3 == false) ? (msj.txt = `El Producto no existe`, msj.status = 404) : ('');

    if (validator == false  && validator3 != false) {

        validator3[0].name = (data.name == false) ? (validator3[0].name) : (data.name);
        validator3[0].price = (data.price == false) ? (validator3[0].price) : (data.price);
        validator3[0].imgUrl = (data.imgUrl == false) ? (validator3[0].imgUrl) : (data.imgUrl);
        validator3[0].ingredients = (data.ingredients == false) ? (validator3[0].ingredients) : (data.ingredients);
        validator3[0].score = (data.score == false) ? (validator3[0].score) : (data.score);
        validator3[0].usersAlreadyScored = (data.usersAlreadyScored == false) ? (validator3[0].usersAlreadyScored) : (data.usersAlreadyScored);




        const productUpdated = await Product.updateMany({ _id: id },
            {
                $set: {
                    name: validator3[0].name,
                    price: validator3[0].price,
                    imgUrl: validator3[0].imgUrl,
                    ingredients: validator3[0].ingredients,
                    score: validator3[0].score,
                    usersAlreadyScored: validator3[0].usersAlreadyScored,
                }
            }
        );
        
        modifyACacheProduct(validator3[0]);
        msj.txt = 'Los datos solicitados han sido actualizados';
        msj.status = 200;
    }
    return msj;
};

//Delete a product
const deleteProduct = async (id) => {
   let msj = {
        txt: '',
        status: 0,
    };

    let validator = await findById(Product, id);;

    if (validator != false) {
        const productDeleted = await Product.deleteOne({ _id: id });
        deleteACacheProduct(id);
        msj.txt = 'Producto eliminado exitosamente';
        msj.status = 200;
    } else {
        msj.txt = 'Producto no encontrado';
        msj.status = 404;
    }
    return msj
};

export{
    createProduct,
    modifyAProduct,
    deleteProduct
};