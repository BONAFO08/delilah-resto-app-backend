import "dotenv/config.js";
import redis from 'redis'
import bluebird from "bluebird";
bluebird.promisifyAll(redis);

const redisClient = redis.createClient({
    host: process.env.ELASTICACHE_URL || process.env.REDIS_HOST,
    port: process.env.REDIS_PORT || 6379
});

redisClient.on('error', (err) => {
    console.error(err);
});

export default redisClient;
